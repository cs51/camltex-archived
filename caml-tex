#! /usr/bin/env python

""" A python version of caml-tex.

Usage:
    - caml-tex [file] [-o outfile] [-c config] [-b wrapin] [-i wrapmid]
        [-l wrapout] [-w textwidth] [--wrapinlist wrapinlist]
        [--wrapmidlist wrapmidlist] [--indent indent] [-a autogobble]
        [-g gobble] [-m stylefile] [-s style] [-e linenumber] [-n] [-u]
        [-p passthrough]
    - defaults:
        outfile = [file].tex;
        config = ''; (no config file to read)
        wrapin = '# {}';
        wrapmid = '  {}';
        wrapout = '{}';
        textwidth = 80;
        wrapinlist = '{}';
        wrapmidlist = '{}';
        indent = ''; (no special indentation)
        autogobble = true;
        gobble = ''; (no gobbling)
        stylefile = [file]-cmlstyle.tex;
        style = default;
        linenumber = false;
        (-n) informat = true;
        (-u) outformat = false;
        passthrough = '';

Known Problems:
    - phrases containing "\n# " are considered by pexpect to be the end of a
    statement.
"""

import re
from argparse import ArgumentParser
from ocaml_eval import OCamlSession
from ocaml_writer import CamlTexFileWriter
import os
from pygments.styles import get_all_styles
from univ import *

class BadMLException(Exception):
    """
    Class to represent exceptions related
    to parsing ML from the .mlt file.
    """

    def __init__(self, message):
        self.message = message
        super(BadMLException, self).__init__()

    def __repr__(self):
        return "BadMLException: {}".format(self.message)


class BadTexException(Exception):
    """
    Class to represent exceptions related
    to parsing TeX from the .mlt file.
    """

    def __init__(self, message):
        self.message = message
        super(BadTexException, self).__init__()

    def __repr__(self):
        return "BadTexException: {}".format(self.message)


def extract_ml_statements(filepointer):
    """
    Extract ML statements from the filepointer. Statements must eventually end
    with a line containing a ";;"; otherwise, a BadTexException will be raised.

    Args:
        filepointer (file): The mlt file to read.

    Returns:
        (str list, bool): A tuple containing OCaml statements and whether or
            not the last line contains ;;
    """

    statements = []

    statement = ""

    while True:
        line = filepointer.readline()

        if line is None:
            raise BadTexException("Opened Caml Statement never closed.")

        elif re.search(END_REGEX, line):
            hang = False
            if statement != "":
                statements.append(statement)
                hang = True

            return statements, hang

        statement += line.rstrip(' \t\r\x0b\x0c')

        end_line = r'(;;\s*$)|(;;(\s*\(\*.*\*\))+$)'

        if re.search(end_line, line.strip()):
            statements.append(statement)
            statement = ""


def make_custom_dict(some_options, is_listing):
    """
    Create an environment-specific options dictionary by merging some_options
    with OPTIONS

    Args:
        some_options (dict): environment-specific options dictionary
        is_listing (bool): whether or not the environment is a listing

    Returns:
        (dict): A dictionary with parts of OPTIONS overriden by some_options.
        some_options will not contain wrap__list, only wrap__.
    """

    def bool_of_string(s):
        return s.lower() == 'true'

    custom_options = {}
    for k in OPTIONS:
        # in a listing, if wraps have been specified, use those; otherwise use
        # wrap__list
        if k in ["wrapin", "wrapmid"]:
            if some_options.get(k) is None:
                if is_listing:
                    val = OPTIONS[k + "list"]
                else:
                    val = OPTIONS[k]
            else:
                val = some_options[k]
        else:
            val = OPTIONS[k] if some_options.get(k) is None else some_options[k]
        # make sure the boolean options indeed have boolean values
        if k in ["linenumber", "informat", "outformat", "autogobble"]:
            if isinstance(val, bool):
                custom_options[k] = val
            else:
                custom_options[k] = bool_of_string(val)
        elif k in ["textwidth"]:
            custom_options[k] = int(val)
        elif "list" in k:
            next
        else:
            custom_options[k] = val

    return custom_options


def is_ocaml_env(line):
    """ Checks if a line begins OCaml code

        Args:
            line (str): the line to parse

        Returns:
            (bool, str, dict): A tuple: True if in an OCaml environment, False
            otherwise; a string indicating the type of environment (or None);
            and a dictionary of options
    """

    if INLINE_VAL.search(line) is not None:
        kind = INLINE_VAL.search(line).group(1)
        return True, kind, None
    else:
        info = re.match(START_REGEX, line)
        if info is not None:
            kind = info.group(1)
            options = info.groups()[1]
            if options is not None:
                try:
                    options = dict([pair.split('=', 1) for pair in
                                    options.split(',')])
                    custom_options = make_custom_dict(options, kind ==
                                                      "listing")
                    return True, kind, custom_options
                except BaseException:
                    print("Bad options given in environment")
            else:
                return True, kind, make_custom_dict({}, kind == "listing")
        else:
            return False, None, None


def convert_to_tex(filename, outfilename):
    """ Convert the MLT file at the path filename
        to a .tex file by converting each block of ocaml to a nicely-formatted
        one and just copying everything else.

        Args:
            filename (str): The file to convert
            outfilename (str): The output file to write to
    """

    # start up and wait for the shell to be ready
    ocaml = OCamlSession(args=OPTIONS["passthrough"])

    # try to open the outfile as a relative path first
    try:
        writer = CamlTexFileWriter(os.getcwd() + '/' + outfilename,
                                   macrofilepath=OPTIONS["stylefile"],
                                   style=OPTIONS["style"])
    except IOError:
        try:
            writer = CamlTexFileWriter(
                outfilename,
                macrofilepath=OPTIONS["stylefile"],
                style=OPTIONS["style"])
        except IOError as excep:
            print("Could not open output file: {}".format(excep))
            exit(1)

    # get the source file and the output file
    try:
        infile = open(filename, 'r')
    except IOError as excep:
        print("Input file error: {}".format(excep))
        exit(1)

    while True:

        line = infile.readline()

        # if line is empty, infile has ended.
        if not line:
            infile.close()
            writer.close()
            return

        # check if the line is tex or ocaml
        is_ocaml, kind, options = is_ocaml_env(line)
        if is_ocaml:
            # inline ocaml handled somewhat separately right now because of the
            # need to split using INLINE_VAL
            if kind == "val" or kind == "string":
                matches = INLINE_VAL.split(line)
                texts = matches[::4]
                statements = matches[3::4]
                outputs = ocaml.parse(statements, kind)
                writer.write_tex(''.join(intertwine(texts, outputs)))
            else:
                statements, hang = extract_ml_statements(infile)
                # refuse to parse the tex if ocaml didn't end in ;; in an
                # evaluation environment
                if kind != "listing" and hang:
                    raise BadTexException("statement doesn't end in ;;")
                # let the evaluator figure out what input/output to return
                inouts = ocaml.parse(statements, kind)
                # if an eval, inouts will be empty, so there is nothing to
                # write.
                if len(inouts) != 0:
                    writer.write_statements(inouts, options)
        else:
            if re.search(DOC_START, line):
                writer.write_styles(OPTIONS["verbpass"])
                writer.write_tex(line)
            else:
                writer.write_tex(line)


parser = ArgumentParser()

parser.add_argument('files', metavar='F', type=str, nargs='+',
                    help='input .mlt file')
parser.add_argument('-o', '--outfile', dest='outfile', default="",
                    help='output .tex file')
parser.add_argument('-c', '--config', dest='config', default="",
                    help='config file containing options')
parser.add_argument('-b', '--wrapin', dest='wrapin', default='# $text',
                    help='Python format string for wrapping input')
parser.add_argument('-i', '--wrapmid', dest='wrapmid', default='  $text',
                    help='Python format string for wrapping middle lines')
parser.add_argument('-l', '--wrapout', dest='wrapout', default='$text',
                    help='Python format string for wrapping output')
parser.add_argument('-w', '--textwidth', dest='textwidth', default='80',
                    help='Width for each line of OCaml')
parser.add_argument('--wrapinlist', dest='wrapinlist', default='$text',
                    help='Python format string for wrapping input')
parser.add_argument('--wrapmidlist', dest='wrapmidlist', default='$text',
                    help='Python format string for wrapping middle lines')
parser.add_argument('--indent', dest='indent', default='',
                    help='default indentation unit')
parser.add_argument('-a', '--autogobble', dest='autogobble',
                    action='store_false', help='turn off autogobbling')
parser.add_argument('-g', '--gobble', dest='gobble', default='',
                    help='default number of characters to gobble')
parser.add_argument('-m', '--stylefile', dest='stylefile',
                    default="-cmlstyle.tex",
                    help='macro .tex file')
parser.add_argument('-s', '--style', dest='style', default='default',
                    choices=list(get_all_styles()),
                    help='style for the pygments formatter')
parser.add_argument('-e', '--linenumber', dest='linenumber',
                    action='store_true', help='turn on line numbering')
parser.add_argument('-n', '--informat', dest='informat',
                    action='store_false', help='turn off input formatting')
parser.add_argument('-u', '--outformat', dest='outformat',
                    action='store_true', help='turn on output formatting')
parser.add_argument('-p', '--passthrough', dest='passthrough', default='',
                    help='arguments to pass to OCaml interpreter')
parser.add_argument('-v', '--verbpass', dest='verbpass', default='',
                    help='arguments to pass to Verbatim environment')

args = parser.parse_args()

for f in args.files:
    if args.outfile is "":
        # if the input file ends in .mlt, the output should strip that
        if f[-4:] == '.mlt':
            out = f[:-4] + '.tex'
        else:
            out = f + '.tex'
    else:
        out = args.outfile

    OPTIONS = {
        "wrapin": args.wrapin,
        "wrapmid": args.wrapmid,
        "wrapinlist": args.wrapinlist,
        "wrapmidlist": args.wrapmidlist,
        "wrapout": args.wrapout,
        "textwidth": int(args.textwidth),
        "stylefile": args.stylefile,
        "style": args.style,
        "linenumber": args.linenumber,
        "informat": args.informat,
        "outformat": args.outformat,
        "savebox": "",
        "gobble": args.gobble,
        "indent": args.indent,
        "autogobble": args.autogobble,
        "verbpass": args.verbpass,
        "passthrough": args.passthrough
    }

    if args.config != "":
        custom_options = {}
        config_file = open(args.config, 'r')
        while True:
            line = config_file.readline()
            if not line:
                config_file.close()
                break
            if line.startswith('#'):  # comment lines start with #
                continue
            if "=" not in line:
                raise SyntaxError("config file has bad line")
            else:
                param, value = line.split("=", 1)
                custom_options[param] = value.rstrip()
        OPTIONS.update(custom_options)

    convert_to_tex(f, out)
