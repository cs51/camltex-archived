# caml-tex

caml-tex is a utility for creating LaTeX files with nicely formatted OCaml code.
It uses pexpect to evaluate OCaml and pygments to format the resulting output.

## Usage

```
./caml-tex [file]
```

with optional arguments:

- `-o` or `--outfile`: the output file to write to; default `[file].tex`, or, if the input file ends in `.mlt`, `([file]-.mlt]).tex`

- `-b` or `--wrapin`: Python format string for first line of OCaml input;
	default `# {}`

- `-i` or `--wrapmid`: Python format string for intermediate lines of OCaml
	input; default `  {}`

- `-l` or `--wrapout`: Python format string for OCaml output; default `{}`

- `-m` or `--stylefile`: suffix of file in which to place the pygments preamble
	formatting; default `-cmlstyle.tex`

- `-s` or `--style`: style to use for the pygments formatter; default `default`;
  the possible choices are `'manni', 'igor', 'lovelace', 'xcode', 'vim',
  'autumn', 'abap', 'vs', 'rrt', 'native', 'perldoc', 'borland'`, `'arduino',
  'tango', 'emacs', 'friendly', 'monokai', 'paraiso-dark', 'colorful',
  'murphy', 'bw', 'pastie', 'rainbow_dash'`, `'algol_nu', 'paraiso-light',
  'trac', 'default', 'algol', 'fruity'`

- `-n` or `--informat`: flag indicating whether or not to format OCaml input; if
  not given, assumed true.

- `-u` or `--outformat`: flag indicating whether or not to format OCaml output;
  if not given, assumed false.
	
## Examples

```
./caml-tex test.mlt -s bw
```

reads the file `test.mlt` and writes to `test.tex` using the `bw` style of
pygments' LaTeX formatter.

```
./caml-tex test.mlt -i "| {}" -l "> {}" -o prompt_test.tex
```

reads the file `test.mlt`, writes to `prompt_test.tex`, and will format code like

```
# let foo = 42 in
| let bar = 10 in
| foo * bar ;;
> - : int = 420
```

See `testfiles/doc.mlt` for an example file that can be compiled.
