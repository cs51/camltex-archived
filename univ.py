import re

# Regular Expressions
DOC_START = re.compile(r"\s*\\begin{document}\s*")
START_REGEX = re.compile(r"\s*\\begin{caml_(example|example\*|eval|listing|process)\s*}(?:\[(.*?)\])?")
END_REGEX = re.compile(r"\\end{caml_(example|example\*|eval|listing|process)}\s*")
INLINE_VAL = re.compile(r"\\caml_(val|string)(\S)(.+?)\2")
MARKUP_VAL = re.compile(r"\\markup(\S)(?P<markup>.*?)\1(?P<ocaml>.*?)\1", flags=re.S)
HIGHLIGHT_VAL = re.compile(r"\\begin{Verbatim}\[commandchars=\\\\\\{\\},codes={\\catcode`\$=3}\]\n(.*?)\n\\end{Verbatim}", flags=re.S)


def intertwine(l1, l2):
    if len(l1) > len(l2):
        return [a for t in zip(l1[:len(l2)], l2) for a in t] + l1[len(l2):]
    else:
        return [a for t in zip(l1, l2[:len(l1)]) for a in t] + l2[len(l1):]

