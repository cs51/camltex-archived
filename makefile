clean:
	rm *.aux *.log *.tex *.pdf *.pyc
	rm testfiles/*.tex testfiles/*.log, testfiles/*.aux

all: record empty test bad error style doc

%:
	./caml-tex testfiles/test_$@.mlt -o testfiles/test_$@.tex
	pdflatex -shell-escape -output-directory=testfiles testfiles/test_$@.tex 

test:
	./caml-tex testfiles/test.mlt -p="-w +a" --wrapin="\textcolor{gray}{# }{\$$text}" -o testfiles/test.tex
	pdflatex -shell-escape -output-directory=testfiles testfiles/test.tex

config_test:
	./caml-tex testfiles/test.mlt -c testfiles/config_test.cfg -o testfiles/config_test.tex
	pdflatex -shell-escape -output-directory=testfiles testfiles/config_test.tex

style:
	./caml-tex testfiles/test.mlt -o testfiles/test_bw.tex -s bw
	pdflatex -shell-escape -output-directory=testfiles testfiles/test_bw.tex

line_test:
	./caml-tex testfiles/test.mlt -o testfiles/line_test.tex -b "{num} {text}" -i "{num} {text}" -e
	pdflatex -shell-escape -output-directory=testfiles testfiles/line_test.tex

pass_test:
	./caml-tex testfiles/test.mlt -p="-w A" -o testfiles/test.tex
	pdflatex -shell-escape -output-directory=testfiles testfiles/pass_test.tex

doc:
	./caml-tex testfiles/doc.mlt -o testfiles/doc.tex
	pdflatex -shell-escape -output-directory=testfiles testfiles/doc.tex
