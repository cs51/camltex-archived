import pexpect
import re
from univ import *
from strings import *


class OCamlSession(object):
    """
    Wrapper around the pexpect class to run an OCaml session.

    Might make it easier to this to be a generalized tool!

    """

    def __init__(self, prompt="# ", args=""):
        """
        Initialize the OCaml session, change prompt if needed, and await input.

        Args:
            prompt (str): prompt for pexpect to await; if not the default OCaml
            prompt, __init__ will change the prompt (eventually).
        """
        self.ocaml = pexpect.spawn('ocaml ' + args, echo=False)
        # expect a newline because ocaml will start with lots of notes to the
        # reader before actually spawning the prompt
        self.ocaml.expect('\n# ')
        self.prompt = prompt
        self.expect = self.prompt
        # TODO: change prompt if desired in the ocaml interpreter?

    def parse(self, statements, kind):
        """
        Essentially a wrapper for evaluate, but decides whether to return
        input, output, or both depending on the given kind.

        Args:
            statements (list): several OCaml statements to be evaluated
            kind (str): the kind of OCaml environment
        """

        ocaml_statements = [self.to_code_object(m) for m in statements]
        raw_statements = [m.string for m in ocaml_statements]

        if kind == "eval":
            for statement in raw_statements:
                # evaluate and return nothing
                self.evaluate(statement)
            return []
        elif kind == "listing":
            # do nothing
            return [(s, "") for s in ocaml_statements]
        elif kind == "val":
            outs = [self.evaluate(s + ' ;;') for s in raw_statements]
            return [o[(o.find('=') + 1):].strip() for o in outs]
        elif kind == "string":
            outs = [self.evaluate(s + ' ;;') for s in raw_statements]
            return [o[(o.find('=') + 1):].strip().strip("\"\'") for o in outs]
        else:
            evals = [self.evaluate(s) for s in raw_statements]
            if kind == "example":
                return [(ocaml_statements[i], evals[i]) for i in range(len(evals))]
            elif kind == "example*":
                return [(ocaml_statements[i], "") for i in range(len(evals))]
            elif kind == "process":
                return [("", evals[i]) for i in range(len(evals))]

    def to_code_object(self, ml_block):
        if MARKUP_VAL.search(ml_block) is not None:
            matches = MARKUP_VAL.finditer(ml_block)
            pos = 0
            c = CodeObject([])
            for match in matches:
                if pos != match.start():
                    c = c.append(TextObject(ml_block[pos:match.start()], ""))
                c = c.append(TextObject(match.group('ocaml'),
                                    match.group('markup')))
                pos = match.end()
            c = c.append(TextObject(ml_block[pos:], ""))
        else:
            c = CodeObject([TextObject(ml_block, "")])
        return c

    # def remove_markup(self, ml_block):
    #     """
    #     Given an ML_block, parse it for possible markups and return only the
    #     OCaml code.
    #
    #     Args:
    #         ml_block (str): a block of OCaml, possibly with markup
    #
    #     Returns:
    #         str: a block of OCaml without markup
    #     """
    #     new_block = ""
    #     if MARKUP_VAL.search(ml_block) is not None:
    #         matches = MARKUP_VAL.finditer(ml_block)
    #         pos = 0
    #         texts = []
    #         ocamls = []
    #         for match in matches:
    #             if pos != match.start():
    #                 texts.append(ml_block[pos:match.start()])
    #             ocamls.append(match.group('ocaml'))
    #             pos = match.end()
    #         texts.append(ml_block[pos:])
    #         return "".join(intertwine(texts, ocamls))
    #     else:
    #         return ml_block

    def evaluate(self, ml_block):
        """
        Given an ML block, evaluate the block, await new input, and return the
        input and output from evaluating the ML block in the toplevel. If an
        OCaml error is thrown, return the error message.

        Args:
            ml_block (str): a block of OCaml to pass to the session. This block
            can include multiple lines of code to evaluate.

        Returns:
            str: the output from evaluation.
        """
        ansi_esc_re = re.compile(r'\x1b\[[0-?]*[ -/]*[@-~]')
        for line in ml_block.split("\n"):
            self.ocaml.sendline(line.strip())
        self.ocaml.expect(self.expect)
        statement = self.ocaml.before

        # determine what part of the statement is reformatted input;
        # uses a search string that allows for whitespace and extra
        # asterisks (inserted for unknown reasons by ocaml) between
        # tokens
        input_pattern_string = "[\s\*]+".join([re.escape(s) for s in ml_block.split()])
        input_pattern = re.compile(input_pattern_string)

        # error and warning messages have this ANSI escape code
        if "\\x1b[" in repr(statement):
            # the buffer is offset, so expect new prompt
            self.ocaml.expect(self.expect)
            statement = self.ocaml.before
            # remove the ANSI
            statement = ansi_esc_re.sub('', statement)
            # remove all occurences of original input
            output_start_result = input_pattern.search(statement)
            if output_start_result == None:
                output_start = 0
                print "Problem finding input pattern:\n[[[%s]]]\nin \n[[[%s]]]\n" % (input_pattern_string, statement)
            else:
                output_start = output_start_result.span()[1]
            err_pattern = \
              re.compile("[\s\*]+"
                            .join(["#"] +
                                [re.escape(s) for s in ml_block.split()]))
            statement = err_pattern.sub("", statement[output_start:])

        output = statement.strip().strip('*')

        return output

    def reset(self):
        """
        Return the session to an clean state by
        resetting the underlying OCaml process.
        """
        self.ocaml_interactive = pexpect.spawn('ocaml')
        self.ocaml_interactive.expect('\n# ')

        # TODO: change the prompt again

        return True

    def __repr__(self):
        return "<OCaml Session>"
