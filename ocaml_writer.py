"""
Wrapper class to write beautiful generic LaTeX files.
"""

from pygments import highlight
from pygments.formatters import LatexFormatter
from pygments.lexers.functional import OcamlLexer
from string import Template
import os
import re
from univ import *
from strings import *


class CamlTexFileWriter(object):
    """
    Wrapper class to manage listings in OCaml.
    """

    def __init__(self, filepath, macrofilepath=None, style="default"):
        self.style = style
        self.LF = LatexFormatter(verboptions=r'codes={\catcode`$=3}',
                                 style=style)
        self.OL = OcamlLexer()
        if macrofilepath is None:
            macrofilepath = "-cmlstyle.tex"
        self.fname = filepath
        self.fpointer = open(filepath, 'w')
        self.macrofname = os.path.splitext(filepath)[0] + macrofilepath
        self.macrofpointer = open(self.macrofname, 'w')
        self.currline = 1

    def set_currline(self, linenum):
        self.currline = linenum

    def get_currline(self):
        """
        Get the current line number, then increment.

        Returns:
            The current line number
        """
        val = self.currline
        self.currline = self.currline + 1
        return val

    def reset_currline(self):
        """
        Set the current line number back to 1 for new blocks of code.
        """
        self.set_currline(1)

    def write_styles(self, options):
        """
        Make it so that the OCaml looks pretty, by outputting the appropriate
        code formatting styles.
        """
        self.macrofpointer.write('\n\\usepackage{fancyvrb,color,bold-extra}\n')
        self.macrofpointer.write(self.LF.get_style_defs())
        # account for custom verbatim options
        self.macrofpointer.write("\\RecustomVerbatimEnvironment\n\t{Verbatim}{Verbatim}\n\t{"
                                 + r"commandchars=\\\{\},codes={\catcode`$=3}"
                                 + ("," + options if options != "" else "")
                                 + "}\n")
        self.macrofpointer.write(r"\newcommand{\usecaml}{\UseVerbatim}" + "\n")
        self.fpointer.write("\input{" + os.path.split(self.macrofname)[1] +
                            "}\n")

    def write_tex(self, line):
        """
        Write an unchanged line of LaTeX to the file.

        Args:
            line (str): the line to write
        """
        self.fpointer.write(line)
        return True

    def ocaml_highlight(self, ocaml):
        """
        Uses pygments to highlight a block of OCaml.

        Args:
            ocaml (str): A block of OCaml

        Returns:
            str: the highlighted block (without begin/end Verbatims)
        """
        if ocaml.strip() != "":
            # because highlight seems to ignore preceding and following
            # whitespace, we manually add it
            left_len = len(ocaml) - len(ocaml.lstrip('\t\r\x0b\x0c\n'))
            right_len = len(ocaml.rstrip('\t\r\x0b\x0c\n'))
            left = ocaml[:left_len]
            right = ocaml[right_len:]
            mid = HIGHLIGHT_VAL.match(highlight(ocaml, self.OL, self.LF))
            return left + mid.group(1) + right
        else:
            return ocaml

    def block_format(self, source, highlight):
        """
        Marks up code and possibly highlights using pygments

        Args:
            source (str): OCaml code to format and highlight with pygments
            highlight (bool): whether or not to highlight

        Returns:
            str: formatted source code: highlighted and marked up
        """
        if not source.strip().is_empty():
            if highlight:
                h_code = CodeObject([TextObject(self.ocaml_highlight(t.text), t.markup) for t in source.text_objects])
            else:
                h_code = CodeObject([t.replace("\\", r"\textbackslash ")
                                      .replace('{', r'\{')
                                      .replace('}', r'\}') if t.markup == "" else t for t in source.text_objects])
            return h_code.formatted()
        else:
            return ""

    def wrap_line(self, line, textwidth):
        """
        Wrap a line with the specified textwidth; line will be at most
        textwidth characters. Will split at spaces if possible and otherwise
        just cut at the [textwidth]th character.

        Args:
            line (str): input line to wrap
            textwidth (int): line length at which to wrap

        Returns:
            A single string with newlines inserted appropriately.
        """
        if isinstance(line, basestring):
            line = line.rstrip()
            if line.strip() != "":
                if len(line) <= textwidth:
                    return line
                else:
                    # split at textwidth and check if either part has spaces
                    part = line[:textwidth]
                    rest = line[textwidth:]
                    if not (part.rstrip() != part or rest.lstrip() != rest):
                        words = part.rsplit(None, 1)
                        # if no spaces under textwidth chars, just split
                        if len(words) == 1:
                            return part + "\n " + self.wrap_line(rest, textwidth)
                        else:
                            # find spaces by splitting into words
                            last = words[-1]
                            # grab part up to its last space
                            revised_part = part[:(-1 * len(last))]
                            rest = part[(-1 * len(last)):] + rest
                            part = revised_part.rstrip()
                            rest = rest.rstrip()
                    else:
                        part = part.rstrip()
                        rest = rest.lstrip()

                    return part + "\n  " + self.wrap_line(rest, textwidth)
            else:
                return ""
        else:
            line = line.rstrip()
            if not line.strip().is_empty():
                if line.length <= textwidth:
                    return line
                else:
                    # split at textwidth and check if either part has spaces
                    part, rest = line.split_at(textwidth)
                    if not (part.rstrip().string != part.string or rest.lstrip().string != rest.string):
                        words = part.string.rsplit(None, 1)
                        # if no spaces under textwidth chars, just split
                        if len(words) == 1:
                            part = part.append(TextObject("\n ", ""))
                            part = part.concat(self.wrap_line(rest, textwidth))
                        else:
                            rev, last = part.split_at(len(words[-1]))

                            rest = last.concat(rest)
                            part = rev.rstrip()
                            rest = rest.rstrip()
                    else:
                        part = part.rstrip()
                        rest = rest.lstrip()

                    new_part = part.append(TextObject("\n  ", ""))
                    new_part = new_part.concat(self.wrap_line(rest, textwidth))
                    return new_part
            else:
                return CodeObject([])

    def wrap_lines(self, lines, textwidth):
        """
        Wrapper around wrap_line for wrapping multiple lines

        Args:
            lines (str list): a list of lines to wrap
            textwidth (int): line length at which to wrap

        Returns:
            A single string with newlines inserted appropriately
        """
        if isinstance(lines[0], basestring):
            return '\n'.join([self.wrap_line(line, textwidth) for line in lines])
        else:
            return CodeObject.join(TextObject("\n", ""), [self.wrap_line(line, textwidth) for line in lines])

    def format_input(self, input, options):
        """
        Format input based on options dictionary; options should contain
        autogobble, gobble, informat, wrapin, wrapmid, linenumber, and
        textwidth keys.

        Args:
            input (str): an input ocaml string
            options (dict): formatting options dictionary

        Returns:
            a string of formatted code (absent of any environment)
        """
        # wrap lines
        input = self.wrap_lines(input.split("\n"), options["textwidth"])
        # raw is a list of lines of ocaml
        raw = [l.rstrip().replace('\t', ' ') for l in input.split("\n")]

        # gobble if necessary
        if options["autogobble"]:
            beg_whitespace = min([len(l.string) - len(l.lstrip().string) for l in raw])
            raw = [l.split_at(beg_whitespace)[1] for l in raw]

        if options["gobble"] != "":
            gobble = int(options["gobble"])
            raw = [l.split_at(gobble)[1] for l in raw]

        # process markup and apply highlighting if necessary
        raw = self.block_format(CodeObject.join(TextObject("\n", ""), raw), options["informat"]).split("\n")

        # add formatting to individual lines
        wrapin = options["wrapin"]
        wrapmid = options["wrapmid"]
        # wrapin and wrapmid should have {num} and {text} if linenumber
        fst = Template(wrapin).safe_substitute(text=raw[0],
                                               num=self.get_currline())
        rest = [Template(wrapmid).safe_substitute(text=s,
                                                  num=self.get_currline()) for s in raw[1:]]

        rest.insert(0, fst)
        return "\n".join(rest).rstrip()

    def format_output(self, output, options):
        """
        Format output based on options dictionary. Options dict should contain
        textwidth, wrapout, and outformat keys.

        Args:
            output (str): unformatted ocaml output
            options (dict): dictionary containing formatting options

        Returns:
            Appropriately formatted output.
        """
        # wrap lines
        output = self.wrap_lines(re.split("(?:\r\n)+", output),
                                 options["textwidth"])
        # apply highlighting if necessary
        if options["outformat"]:
            raw = self.ocaml_highlight(output).split("\n")
        else:
            # escape braces so LaTeX outputs them
            raw = [l.rstrip().replace('\\', r"\textbackslash ")
                   .replace('{', '\{').replace('}', '\}')
                   for l in output.split("\n")]

        # add formatting to individual lines
        wrapout = options["wrapout"] if "$text" in options["wrapout"] else "$text"
        out = [Template(wrapout).safe_substitute(text=s) for s in raw]

        return "\n".join(out).rstrip()

    def write_statements(self, inouts, options):
        """
        Writes a list of pairs of input/output ocaml with appropriate options
        to the file.

        Args:
            inouts ((str, str) list): a list of pairs of ocaml input and output
            options (dict): dictionary of formatting options
        """

        env = "Verbatim" if options["savebox"] == "" else "SaveVerbatim"

        # currently the only fancyvrb option is indentation, but if we need
        # future ones they cn be dealt with here.
        fancyvrb_options = []

        if options["indent"] != "":
            fancyvrb_options.append("xleftmargin=" + options["indent"])

        if options["savebox"] != "":
            fancyvrb_options.append(r"commandchars=\\\{\},codes={\catcode`$=3}")
            if options["verbpass"] != "":
                fancyvrb_options.append(options["verbpass"])
            str_options = "[" + ",".join(fancyvrb_options) + "]{" + \
                options["savebox"] + "}"
        else:
            str_options = "[" + ",".join(fancyvrb_options) + "]" if \
                fancyvrb_options != [] else ""

        self.write_tex(r"\begin{" + env + "}" + str_options + "\n")

        for pair in inouts:
            self.write_ocaml(pair, options)
            self.reset_currline()
        self.write_tex(r"\end{" + env + "}\n")

    def write_ocaml(self, inout, options):
        """
        Write stylized OCaml to the output file, styling with pygments at the
        same time.

        Args:
            inout (str, str): An input/output tuple for a single ocaml block
            options (dict): dictionary of formatting options
        """
        # preserve whitespace between input statements
        formatted_input = formatted_output = ""
        if inout[0] != "":
            left_white, text = inout[0].split_at(len(inout[0].string) - len(inout[0].string.lstrip("\n\r\x0b\x0c")))
            inout = (text.rstrip(), inout[1].strip())

            if not inout[0].is_empty():
                formatted_input = left_white.string + self.format_input(inout[0], options)

        if inout[1] != "":
            formatted_output = self.format_output(inout[1], options)

        # write the ocaml (environments handled in write_statements)
        if formatted_input != "" or formatted_output != "":
            if formatted_input != "" and formatted_output != "":
                self.fpointer.write(formatted_input + "\n" + formatted_output)
            elif formatted_input != "":
                self.fpointer.write(formatted_input)
            elif formatted_output != "":
                self.fpointer.write(formatted_output)
        self.fpointer.write("\n")

    def close(self):
        """Close the file writer"""
        self.fpointer.close()

    def __repr__(self):
        return "<CamlWriter {}>".format(self.fname)
