# Pygments style for CS 51 OCaml highlighting

from pygments.style import Style
from pygments.token import Keyword, Name, Comment, String, Error, \
     Number, Operator, Generic

class SimpleStyle(Style):
    default_style = ""
    styles = {
        Comment:                'italic #888',

        Keyword:                   'bold',
        Keyword.Pseudo:            'nobold',
        Keyword.Type:              'bold #333',

        Name.Variable:             "bold #888",

        String:                 '#888'
    }
