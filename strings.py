from string import Template
import math


def get_index(num, lst):
    """
    Given an ordered list, find the index i s.t. lst[i] <= num < lst[i + 1].
    Throws an error if the number is too small.

    Args:
        num (int): the number to locate
        lst (int list): the list in which to locate the number.
    """
    left = 0
    right = len(lst) - 1
    while left <= right:
        mid = int(math.floor((left + right) / 2))
        if (mid == len(lst) - 1 or num < lst[mid + 1]) and lst[mid] <= num:
            return mid
        elif lst[mid] < num:
            left = mid + 1
        elif lst[mid] > num:
            right = mid - 1
        else:
            raise IndexError("num doesn't compare properly with lst[mid]")
    raise IndexError("num too small")


class TextObject:
    """
    Text objects meant to pair pieces of text with markup.

    Their bare text, markup, and formatted versions can be retrieved; minimal
    manipulation through the split function.
    """
    def __init__(self, text, markup):
        """
        Makes a text object with the corresponding text and markup.

        Args:
            text (str): text (probably a code snippet)
            markup (str): Template string corresponding to LaTeX markup of the
            text.
        """
        self.text = text
        self.markup = markup

        # string: formatted version using the Template substitution
        self.formatted = Template(self.markup).safe_substitute(text=self.text) if markup != "" else self.text

        # int: length of the text only.
        self.length = len(self.text)

    def split_at(self, index):
        """
        Splits the text at the specified index and returns two text objects,
        each of which inherit the original's markup.

        Args:
            index (int): index at which to split; numbering works the same as
            string indexing.
        """
        p1 = self.text[:index]
        p2 = self.text[index:]
        return (TextObject(p1, self.markup), TextObject(p2, self.markup))

    def replace(self, old, new):
        return TextObject(self.text.replace(old, new), self.markup)

    def __str__(self):
        """
        String representation; returns a tuple of the text and markup without
        formatting. Use self.formatted to retrieve formatted version instead.
        """
        return "({}, {})".format(self.text, self.markup)
    
    def strip(self):
        return TextObject(self.text.strip(), self.markup)


class CodeObject:
    """
    List of text objects meant to correspond to lines of OCaml code with
    markup.

    The full text-only versions and the formatted versions can be retrieved.
    Can be split and concatenated.
    """
    def __init__(self, text_objects):
        """
        Initializes a code object from a list of text objects. Calculates the
        lengths of the text objects in order to facilitate splitting.

        Args:
            text_objects (TextObject list): list of text objects to group
            together.
        """
        self.text_objects = text_objects
        self.string = "".join([t.text for t in text_objects])

        # int: length of the non-formatted string
        self.length = len(self.string)

        # int list: individual length of each text object
        self.lengths = [t.length for t in text_objects]

        # int list: cumulative lengths of text objects; begins at 0 and ends
        # with total length.
        self.cum_lengths = [0]
        for i in range(len(self.lengths)):
            self.cum_lengths.append(self.cum_lengths[-1] + self.lengths[i])

    def __str__(self):
        """
        String representation: a list of tuples, each corresponding to a text
        object.
        """
        return str([str(t) for t in self.text_objects])

    def formatted(self):
        """
        Format the text objects using Template substitution.
        """
        return "".join([t.formatted for t in self.text_objects])

    def is_empty(self):
        return self.string == ""

    def append(self, t):
        """
        Append the text object t to the code object, returning the new object.

        Args:
            t (TextObject): text object to add.
        """
        if t.text == "":
            return self
        else:
            return CodeObject(self.text_objects + [t])

    def concat(self, c):
        """
        Concatenate the code object c to this code object by concatenating the
        necessary lists.

        Args:
            c (CodeObject): the code object to join.
        """
        if c.is_empty():
            return self
        else:
            return CodeObject(self.text_objects + c.text_objects)

    def char_at(self, index):
        """
        Retrieve the (index)th character of the text-only version.

        Args:
            index (int): index of character to get; numbering is same as
            strings.
        """
        i = get_index(index, self.cum_lengths)
        if i == len(self.cum_lengths) - 1:
            raise IndexError("string index out of range")

        return self.text_objects[i].text[index - self.cum_lengths[i]]

    def split_at(self, index):
        """
        Split at the (index)th character and return two code objects.

        Args:
            index (int): index to split at; numbering is same as strings.
        """
        # locate the correct text object at which to split.
        i = get_index(index, self.cum_lengths)
        if i == len(self.cum_lengths) - 1:
            return (self, CodeObject([]))

        if self.cum_lengths[i] == index:
            # no splitting; just split text_objects at i
            begin = CodeObject(self.text_objects[:i])
            end = CodeObject(self.text_objects[i:])
            return (begin, end)
        else:
            # split text_objects[i] at appropriate index, concatenate
            split_object = (self.text_objects[i]).split_at(index - self.cum_lengths[i])
            begin = CodeObject(self.text_objects[:i])
            begin = begin.append(split_object[0])
            end = CodeObject([split_object[1]])
            end = end.concat(CodeObject(self.text_objects[(i + 1):]))
            return (begin, end)

    def split(self, char):
        curr = self
        text_split = self.string.split(char)
        res = []
        l_char = len(char)
        for i in range(len(text_split)):
            part = curr.split_at(len(text_split[i]))[0]
            rest = curr.split_at(len(text_split[i]) + l_char)[1]
            res.append(part)
            curr = rest
        return res

    def replace(self, old, new):
        return CodeObject([t.replace(old, new) for t in self.text_objects])

    def rstrip(self):
        """
        Return object with no whitespace on the right.
        """
        index = len(self.string.rstrip())
        return self.split_at(index)[0]

    def lstrip(self):
        """
        Return object with no whitespace on the left.
        """
        index = self.length - len(self.string.lstrip())
        return self.split_at(index)[1]

    def strip(self):
        """
        Return object with no whitespace on either side
        """
        return self.rstrip().lstrip()

    @staticmethod
    def join(sep, lst):
        curr = CodeObject([])
        for i in range(len(lst) - 1):
            curr = curr.concat(lst[i])
            curr = curr.append(sep)
        curr = curr.concat(lst[-1])
        return curr
